<?php
/* class for pen description*/
	class Pen {		
		public $name;
		public $life_time;		/* how much inc is left in %*/
		private $triger;		/* determine if pen is on/off (true/false) initial = off */
		public $color;		/* array of the colors*/
		private $color_current;

		/* switch the pencil state */
		function penTrigger() {
			$this->triger = !$this->triger;
		}
		
		/* check the status of the pen (true = on / false = off) */
		function isReady () {
			return (
				($this->life_time != 0) &&
				($this->triger)
			);
		}

		/* return current color*/
		function getColor () {
			return $this->color_current;
		}
		
		/* set new color */
		function setColor ($color_new) {
			if (in_array($color_new, $this->color)) {
				$this->color_current = $color_new;
				return true;
			} else {
				return false;
			}
		}

		function __construct ($name, $life_time, $triger, $color, $color_new) {
			$this->name = $name;
			if (($life_time >= 0) && ($life_time <=1)) {
				$this->life_time = $life_time;
			} else {
				$this->life_time = 0;	/* if life_time is imposible*/
			}
			if (is_bool($triger)) {
				$this->triger = $triger;
			}
			$this->color = $color;
			if (in_array($color_new, $this->color)) {
				$this->color_current = $color_new;
			}
		}
	}
?>